# Error Variable Refresh
$Error.Clear()

# Master Variable Refresh
$PowerShell = [PowerShell]::Create()
$PowerShell.AddScript('Get-Variable | Select-Object -ExpandProperty "Name"') | Out-Null
$VariableList = $PowerShell.Invoke()
$PowerShell.Dispose()
Remove-Variable (Get-Variable | Select-Object -ExpandProperty "Name" | Where-Object { $VariableList -NotContains $_ }) -ErrorAction "SilentlyContinue"

# Console Title
$Host.UI.RawUI.WindowTitle = "Windows PowerShell: Folder Compression"

# Define Error Verbose
$ErrorActionPreference = "SilentlyContinue"

# Character Display Correction
$OutputEncoding = [Console]::InputEncoding = [Console]::OutputEncoding = New-Object System.Text.UTF8Encoding

# Console Screen Refresh
[System.Console]::Clear()

while ($TerminateMode -NE $True) {

    while ((!($RunningMode -EQ $True)) -AND ($TerminateMode -NE $True)) {

        if ([String]::IsNullOrEmpty($InitialExecution)) {

            # Define Variable
            $InitialExecution = $True

            # System Tray Message Maximum Length
            # $TrayIcon.Text = "———————————————————" + "`n" + "• ———————————————————" + "`n" + "• ———————————————————"

            # System Tray Icon Indicator
            if ([String]::IsNullOrEmpty($TrayIcon)) {
                [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null
                [System.Reflection.Assembly]::LoadWithPartialName("System.Drawing") | Out-Null
                $TrayIcon = New-Object System.Windows.Forms.NotifyIcon
                $TrayIcon.Icon = [System.Convert]::FromBase64String("AAABAAIAEBAAAAEAIABoBAAAJgAAACAgAAABACAAqBAAAI4EAAAoAAAAEAAAACAAAAABACAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANt8SBPefkkc339JHOB/SRzggEkc4IBJHOGBSRzhgUkc4oFJHOOCShzkgksc5IJLHO2HThr8k1QG+ZBTAAAAAADef0ip4YBK5d9/SePdfUfj2nxG49h7RuPVekXj03hE49B2QuPOdEHjzXNB4810QuTTeEXf6oZMWN9/SQD+kVMA339JseWCS//igUr/24tc/86BU//Rd0P/0XdD/89/Uv/JimT/xohk/8SHY/+8d07/wG4+/9t9R5z/sWYE/5JTANt9SIDng0v95odQ//LNuP/q3NL/zJNw/8xzQP/kro//9u3o//Xs5//17un/1ayU/71rPP/UeUXJ/5NVE/WMUADXe0ZM54RL7+mFTP/tpHv/+ubb//Xw7P/UrZT/zohf/96eev/doH3/2p17/8uBV/+/bT7/znZD6e+JTi/mhEsA0ndFIuWDS9vth03/6IRK/+iTYv/1z7r/+/f0/+HLvf/Ihl7/ynI//8tyQP/HcUD/w28//8dyQfjqhkxb4IBJAL1rPg7jgUqz74lO/+yHTf/og0r/5YdS/+61lP/77+j/7+Xf/8yYef/MdUP/y3RC/8dyQf/HcUH/4IBJiE8tFwD/wW0A3X5IiPGJTv/wiU7/7IdN/+eDSv/jgUn/776i///////mxLD/0XlH/892Q//LdEL/x3JB/9h7RrP/mFcO3X5IANV5RVvxiU/484tP/++JTv/rhUv/7aJ3//vt5f/34dX/4ZNn/9Z6Rf/SeET/znZD/8pzQv/VekXb74lOItd7RgDPdkQv7IdN6feNUf/zi0//86R4//zr4v/449j/55dp/95+R//afUf/1npG/9J4RP/OdUP/0XdE7+eES0zIckEAvGs9E+iFTMn7j1H/+JVd//3m2P/87OP/7qF0/+aCSf/igUr/3n9I/9p8R//WekX/0XdE/9F3RP3igUqAvmw+AIdNKwTjgUqc+49S//uVWv/7v53/9aV3/+6HTP/qhkz/5oNL/+KBSv/dfkj/2XxH/9V6Rf/SeET/3X5Isb9tPgDef0gA03hFWOqGTN/wiU7k7YZM4+uFS+PohUzj5oNL4+SCSuPhgUnj339J491+SOPafUfj2HtG5d+ASakAAAAAwW4+ALtrPAbQd0Qa2nxIHNp8RxzafUcc231HHNx+Rxzcfkcc3X5IHN5/SBzef0gc339IHOCASRzlg0sTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP//AAAAAwAAAAMAAAABAAAAAQAAAAEAAAABAAAAAQAAgAAAAIAAAACAAAAAgAAAAIAAAADAAAAAwAAAAP//AAAoAAAAIAAAAEAAAAABACAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADXe0ZQ3H1I/99/Sf/ff0n/339J/99/Sf/ff0n/339J/99/Sf/ef0n/3n9I/95/SP/ef0j/3n9I/95/SP/ef0j/3n9I/95/SP/ef0j/3n9I/95/Sf/ff0n/339J/99/Sf/ff0n/7ohO//6RU2AAAAAAAAAAAAAAAAAAAAAAAAAAANV6RYDjgkr/5IJK/+KBSv/ggEn/3n9I/9x9SP/afEf/13tG/9V6Rf/UeUX/0XdE/892Q//NdUP/y3RC/8lzQf/HckH/xXBA/8NvP//Bbj//v20+/79tPv+/bT7/v20+/79tPv+/bT7/8YpP7wAAAAAAAAAAAAAAAAAAAAAAAAAA1HlFcOOBSv/mg0v/5IJK/+KBSv/ggEn/3n9I/9t9R//ZfEf/13tG/9V6Rf/TeUX/0XdE/892Q//NdUP/y3RC/8lzQf/HckH/xXBA/8NvP//Bbj//v20+/79tPv+/bT7/v20+/79tPv/ff0n//pFTMAAAAAAAAAAAAAAAAAAAAADSeEQw3n9I/+iETP/mg0v/5IJK/+GBSf/ff0n/zpJt/7F0Tf/CcD7/13tG/9V6Rf/TeEX/0XdE/892Q//PhFn/u4dm/7qHZv+6h2b/uYZm/7mGZv+4hmX/sXtY/7FmOP+/bT7/v20+/892Q//+kVNgAAAAAAAAAAAAAAAAAAAAAAAAAADZfEf/6YVM/+eES//lg0v/44JK/+64mf///////////8GfiP+wZjf/13tG/9V5Rf/TeEX/0XdE//z28///////////////////////////////////////qnBL/79tPv+/bT7/w28///6RU58AAAAAAAAAAAAAAAAAAAAAAAAAANJ4Rb/rhk3/6YVM/+eES//lg0v/8cCl/////////////////93Kvf+lZz7/yXNB/9R5Rf/TeEX///////////////////////////////////////////+/e1P/v20+/79tPv+/bT7/8YlP3wAAAAAAAAAAAAAAAAAAAAAAAAAAz3ZDgOmFTP/rhk3/6YVM/+eES//nilb/+ODS//////////////////j08v+xgmL/uGo6/9R5Rf/bkWf/6Lui/+e6of/muqH/5bmg/+S5oP/juKD/05Rw/8JvP//AbT7/v20+/79tPv/mhEv//pFTEAAAAAAAAAAAAAAAAAAAAADNdUNQ4oFK/+2HTf/rhk3/6YVM/+eES//lgkv/8cCl///////////////////////PtaP/pmE0/9B3Q//SeET/0HdE/851Q//MdEL/ynNC/8hyQf/GcUD/xHBA/8FuP/+/bT7/v20+/9d7Rv/+kVNQAAAAAAAAAAAAAAAAAAAAAMx0QhDcfUj/74hO/+2HTf/qhkz/6YVM/+eES//kgkr/6Jls//vv6P/////////////////q39f/qndV/8NwP//SeET/0HdE/851Q//MdEL/ynNC/8dyQf/FcUD/w3A//8FuP/+/bT7/y3RC//6RU4AAAAAAAAAAAAAAAAAAAAAAAAAAANN4Rd/wiU7/7ohO/+yHTf/qhkz/6IVM/+aDS//kgkr/5IlV//bXxv/////////////////49PL/wZ+I/65lN//SeET/0HZE/851Q//LdEL/yXNB/8dyQf/FcUD/w28//8FuP/+/bT7/+I5RvwAAAAAAAAAAAAAAAAAAAAAAAAAAynNCn/CJTv/wiU7/7ohO/+yHTf/qhkz/6IRM/+aDS//kgkr/4oFK/+ywjf/99/T/////////////////3cq9/6RmPv/FcUD/z3ZD/811Q//LdEL/yXNB/8dyQf/FcED/w28//8FuP//th03/AAAAAAAAAAAAAAAAAAAAAAAAAADIckFw6YVM//KKT//wiU7/7ohO/+yHTf/qhkz/6IRM/+aDS//kgkr/4oFK/+aYa//5593/////////////////+PTy/6dvSf/Rd0T/z3ZD/811Q//LdEL/yXNB/8dyQf/FcED/w28//95/SP/7j1IwAAAAAAAAAAAAAAAAAAAAAMdxQTDef0j/9ItQ//KKT//wiU7/7ohO/+yHTf/qhUz/6IRM/+aDS//kgkr/4YFJ/99/Sf/zz7r/////////////////zKyX/9N4Rf/Rd0T/z3ZD/811Q//LdEL/yXNB/8dxQf/EcED/0HdE//qPUnAAAAAAAAAAAAAAAAAAAAAAAAAAANJ4RP/2jFD/9ItQ//KKT//wiU7/7YhN/+uGTf/phUz/54RL/+WDS//jgkr/44hV//nn3f/////////////////Wj2X/1XlF/9N4Rf/Rd0T/z3ZD/811Q//KdEL/yHJB/8ZxQP/IckH/+I5RnwAAAAAAAAAAAAAAAAAAAAAAAAAAyXNBv/iNUf/2jFD/84tP//GKT//viU7/7YdN/+uGTf/phUz/54RL/+WDS//44NL/////////////////6K6M/9l8R//Xe0b/1HlF/9N4Rf/Rd0T/znZD/8x1Qv/Kc0L/yHJB/8ZxQP/sh03fAAAAAAAAAAAAAAAAAAAAAAAAAADDbz+A74lO//eNUf/1jFD/84tP//GKT//viU7/7YdN/+uGTf/phUz/9tG8/////////////////+uvjf/dfkj/2n1H/9h8Rv/Wekb/1HlF/9J4RP/Qd0T/znZD/8x0Qv/Kc0L/yHJB/+SCSv/2jFAQAAAAAAAAAAAAAAAAAAAAAMFuP1Dlg0v/+Y5R//eNUf/1jFD/84tP//GKT//viE7/7YdN//fSvP/////////////////tsI7/4IBJ/95/SP/cfkj/2n1H/9h7Rv/Wekb/1HlF/9J4RP/Qd0T/znVD/8x0Qv/Kc0L/2XxH//SLUFAAAAAAAAAAAAAAAAAAAAAAwG4+ENd7Rv/7j1L/+Y5R//eNUf/1jFD/84tP//GJT//5073/////////////////5ayL/+SCSv/igUr/4IBJ/95/SP/cfkj/2n1H/9h7Rv/Wekb/1HlF/9J4RP/Qd0T/znVD/8x0Qv/PdkP/84tPgAAAAAAAAAAAAAAAAAAAAAAAAAAAzXVD3/2QU//7j1L/+Y5R//eNUf/1jFD/+syy//////////////////fSvP/ohUz/5oNL/+SCSv/igUr/4IBJ/95/SP/cfkj/2nxH/9h7Rv/Wekb/1HlF/9J4RP/QdkT/znVD/8t0Qv/uiE6/AAAAAAAAAAAAAAAAAAAAAAAAAAC/bT6f+o9S//2QU//7j1L/+Y5R//aNUP/////////////////50r3/7IdN/+qGTP/ohEz/5oNL/+SCSv/igUr/4IBJ/95/SP/cfUj/2nxH/9d7Rv/VekX/03lF/9F3RP/PdkP/zXVD/+WDS/8AAAAAAAAAAAAAAAAAAAAAAAAAAL9tPmDuiE7//pFT//2QU//6j1L/+I5R//7q3v//////+tO9//CJTv/uiE7/7IdN/+qGTP/ohEz/5oNL/+SCSv/igUr/4IBJ/91+SP/bfUf/2XxH/9d7Rv/VekX/03lF/9F3RP/PdkP/3n9J/++JTjAAAAAAAAAAAAAAAAAAAAAAv20+MN9/Sf/+kVP//pFT//yQUv/6j1L/+I5R//iicf/0i1D/8opP//CJTv/uiE7/7IdN/+qFTP/ohEz/5oNL/+OCSv/hgUn/339J/91+SP/bfUf/2XxH/9d7Rv/VekX/03hF/9F3RP/Xe0b/7ohOcAAAAAAAAAAAAAAAAAAAAAAAAAAAzHRC7/6RU//+kVP//pFT//yQUv/6j1L/+I1R//aMUP/0i1D/8opP//CJTv/tiE3/64ZN/+mFTP/nhEv/5YNL/+OCSv/hgEn/339J/91+SP/bfUf/2XxH/9d7Rv/VeUX/03hF/9h7Rv/sh02AAAAAAAAAAAAAAAAAAAAAAAAAAAC/bT5gz3ZD/99/Sf/ff0n/339J/99/Sf/ef0n/3n9I/95/Sf/ef0j/3n9I/95/SP/ef0j/3n9I/95/SP/ef0j/3n9I/95/SP/ef0j/3n9I/95+SP/ef0j/3n5I/91/SP/dfkj/44JK/+uGTVAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP////////////////////8AAAAfAAAAHwAAAA8AAAAPgAAAD4AAAA+AAAAHgAAAB4AAAAfAAAAHwAAAB8AAAAPAAAAD4AAAA+AAAAPgAAAB4AAAAeAAAAHwAAAB8AAAAfAAAADwAAAA+AAAAPgAAAD/////////////////////")
                $TrayIcon.Text = "Folder Compression" + "`n" + "• Windows PowerShell" + "`n" + "• Terminal Running"
                $TrayIcon.Visible = $True
            }

        }

        # User Doing Nothing (Idle Mode)
        if (!([System.Console]::KeyAvailable)) {

            # Variable Error Detection
            if (($UserInputError -EQ $True) -AND ($LastKeyPress -EQ "F1")) {

                # Automatic [F1] Key Press
                [Void][System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
                [System.Windows.Forms.SendKeys]::SendWait("{F1}")

            }

            # Variable Error Detection
            if (($UserInputError -EQ $True) -AND ($LastKeyPress -EQ "F2")) {

                # Automatic [F2] Key Press
                [Void][System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
                [System.Windows.Forms.SendKeys]::SendWait("{F2}")

            }

            # Console Screen Refresh
            [System.Console]::Clear()

            # Console Line
            "—" * $Host.UI.RawUI.WindowSize.Width

            # Console Information
            Write-Host "Key Press Detection"
            Write-Host "Welcome to a script that will compress a folder into an archive."
            Write-Host "You can uncompress an archive using " -NoNewLine
            Write-Host "Archive Extraction Tool" -ForegroundColor "Yellow" -NoNewLine
            Write-Host "."
            Write-Host "PowerShell must be selected for [F1] - [F12] to operate."

            # Key Press [F1] Selection Information
            if ([String]::IsNullOrEmpty($TargetPath)) {
                Write-Host "• Press [F1] to set source folder location." -ForegroundColor "Red"
            } else {
                Write-Host "• Press [F1] to set source folder location."
            }

            # Key Press [F2] Selection Information
            if ([String]::IsNullOrEmpty($TargetDestination)) {
                Write-Host "• Press [F2] to set target file location." -ForegroundColor "Red"
            } else {
                Write-Host "• Press [F2] to set target file location."
            }

            # Key Press [F3] Selection Information
            if ([String]::IsNullOrEmpty($CompressionLevel)) {
                Write-Host "• Press [F3] to set compression mode." -ForegroundColor "Red"
            } else {
                Write-Host "• Press [F3] to set compression mode."
            }

            # Key Press [F4] Selection Information
            if ([String]::IsNullOrEmpty($ValidateMode)) {
                Write-Host "• Press [F4] to enable validation mode." -ForegroundColor "Red"
            } elseif ($ValidateMode -EQ "Enabled") {
                Write-Host "• Press [F4] to enable strict matching."
            } elseif ($ValidateMode -EQ "Strict") {
                Write-Host "• Press [F4] to disable validation mode."
            }

            # Key Press [F5] Selection Information
            if (([String]::IsNullOrEmpty($DeleteMode)) -AND ([String]::IsNullOrEmpty($RecycleMode))) {
                Write-Host "• Press [F5] to enable deletion mode." -ForegroundColor "Red"
            } elseif (($DeleteMode -EQ $True) -AND ([String]::IsNullOrEmpty($RecycleMode))) {
                Write-Host "• Press [F5] to enable " -NoNewLine
                Write-Host "Recycle Bin" -ForegroundColor "Yellow" -NoNewLine
                Write-Host " mode."
            } else {
                Write-Host "• Press [F5] to disable deletion mode."
            }

            # Key Press [F6] Selection Information
            if ([String]::IsNullOrEmpty($TargetPath)) {
                Write-Host "• Press [F6] to activate script [source folder required]." -ForegroundColor "Red"
            } else {
                Write-Host "• Press [F6] to execute archive creation."
            }

            # Console Line
            "—" * $Host.UI.RawUI.WindowSize.Width

            # Console Information
            Write-Host "Defined Variable Information"

            # Key Press [F1] Variable Detection
            if (!([String]::IsNullOrEmpty($TargetPath))) {
                Write-Host "• Source Folder: $TargetPath" -ForegroundColor "Yellow"
            } else {
                Write-Host "• Source Folder: None"
            }

            # Key Press [F2] Variable Detection
            if (!([String]::IsNullOrEmpty($TargetDestination))) {
                Write-Host "• Target Destination: $TargetDestination" -ForegroundColor "Yellow"
            } else {
                Write-Host "• Target Destination: None"
            }

            # Key Press [F3] Variable Detection
            if (!([String]::IsNullOrEmpty($CompressionMode))) {
                Write-Host "• Compression Level: $CompressionMode" -ForegroundColor "Yellow"
            } else {
                Write-Host "• Compression Level: None"
            }

            # Key Press [F4] Variable Detection
            if (!([String]::IsNullOrEmpty($ValidateMode))) {
                Write-Host "• Validation Mode: $ValidateMode" -ForegroundColor "Yellow"
            } else {
                Write-Host "• Validation Mode: Disabled"
            }

            # Key Press [F5] Variable Detection
            if ((!([String]::IsNullOrEmpty($DeleteMode))) -AND ([String]::IsNullOrEmpty($RecycleMode))) {
                Write-Host "• Source Deletion: Enabled" -ForegroundColor "Yellow"
            } elseif (([String]::IsNullOrEmpty($DeleteMode)) -AND (!([String]::IsNullOrEmpty($RecycleMode)))) {
                Write-Host "• Source Deletion: Recycling" -ForegroundColor "Yellow"
            } elseif (([String]::IsNullOrEmpty($DeleteMode)) -AND ([String]::IsNullOrEmpty($RecycleMode))) {
                Write-Host "• Source Deletion: Disabled"
            }

            # Required Variables are Entered
            if (!([String]::IsNullOrEmpty($TargetPath))) {
                Write-Host "• Execution Mode: Unlocked" -ForegroundColor "Yellow"
            } else {
                Write-Host "• Execution Mode: Locked"
            }

            # Target Source is Missing
            if (!(([System.IO.DirectoryInfo]$TargetPath).Exists)) {
                Remove-Variable -Name "TargetPath" -ErrorAction "SilentlyContinue"
            }

            # Console Line
            "—" * $Host.UI.RawUI.WindowSize.Width

            # Console Pause
            [Threading.Thread]::Sleep(500)

        }

        # Key Press Detection
        if ([System.Console]::KeyAvailable) {

            # User Pressing Defined Key
            switch ([System.Console]::ReadKey().Key) {

                "F1" {

                    # Define Last Key
                    $LastKeyPress = "F1"

                    # Console Screen Refresh
                    [System.Console]::Clear()

                    # Console Line
                    "—" * $Host.UI.RawUI.WindowSize.Width

                    # Console Information
                    Write-Host "User Action Required"
                    Write-Host "Please enter the location of the source folder"
                    Write-Host "that will be compressed into a zipped archive."
                    Write-Host '• $env:UserProfile\Desktop\Example Folder'
                    Write-Host '• $env:UserProfile\Desktop\Compressed'

                    # Variable Error Detection
                    if ($UserInputError -EQ $True) {

                        # Error Mode Refresh
                        Remove-Variable -Name "UserInputError" -ErrorAction "SilentlyContinue"

                        # Console Line
                        "—" * $Host.UI.RawUI.WindowSize.Width

                        # Console Information
                        Write-Host "Warning Notification" -ForegroundColor "Yellow"
                        Write-Host "You have entered a folder path that doesn't exist." -ForegroundColor "Yellow"
                        Write-Host "Please enter the full path of an existing folder." -ForegroundColor "Yellow"

                    }

                    # Console Line
                    "—" * $Host.UI.RawUI.WindowSize.Width

                    # Grab User Input as Variable
                    $TargetPath = Read-Host -Prompt "Select Folder Path"

                    # User Input is Empty Variable
                    if ([String]::IsNullOrWhiteSpace($TargetPath)) {
                        Remove-Variable -Name "TargetPath" -ErrorAction "SilentlyContinue"
                    }

                    # User Input is Not Empty Variable
                    if (!([String]::IsNullOrWhiteSpace($TargetPath))) {

                        # Expand User Input Variable
                        $TargetPath = [System.IO.Path]::GetFullPath($ExecutionContext.InvokeCommand.ExpandString($TargetPath))

                        # Set Current Directory as Default
                        if ($TargetPath.ToLower().Contains("$env:SystemRoot\System32".ToLower())) {
                            $TargetPath = [System.IO.Path]::GetFullPath($ExecutionContext.InvokeCommand.ExpandString($PWD.Path)) + "\" + (Split-Path -Path $TargetPath -Leaf)
                        }

                        # Folder Path Validation
                        if (!(($TargetPath -Match [Regex]::New("^[a-zA-Z0-9\ \:\'\-\[\\\]{}()+=_.,;@~#!£$€¥%^&×¤]+$")) -AND (!($TargetPath.Contains("\\"))))) {
                            Remove-Variable -Name "TargetPath" -ErrorAction "SilentlyContinue"
                            $UserInputError = $True
                        }

                        # Folder Existance Validation
                        if (!(([System.IO.DirectoryInfo]$TargetPath).Exists)) {
                            Remove-Variable -Name "TargetPath" -ErrorAction "SilentlyContinue"
                            $UserInputError = $True
                        }

                    }

                }

                "F2" {

                    # Define Last Key
                    $LastKeyPress = "F2"

                    # Console Screen Refresh
                    [System.Console]::Clear()

                    # Console Line
                    "—" * $Host.UI.RawUI.WindowSize.Width

                    # Console Information
                    Write-Host "User Action Required"
                    Write-Host "Please enter the location of the zipped file"
                    Write-Host "that the source folder will be compressed to."
                    Write-Host "Note that this will default to parent folder."
                    Write-Host '• $env:UserProfile\Desktop\Example File.zip'
                    Write-Host '• $env:UserProfile\Desktop\Compressed.zip'

                    # Variable Error Detection
                    if ($UserInputError -EQ $True) {

                        # Error Mode Refresh
                        Remove-Variable -Name "UserInputError" -ErrorAction "SilentlyContinue"

                        # Console Line
                        "—" * $Host.UI.RawUI.WindowSize.Width

                        # Console Information
                        Write-Host "Warning Notification" -ForegroundColor "Yellow"
                        Write-Host "You have entered an invalid file path." -ForegroundColor "Yellow"
                        Write-Host "Please enter a file path that could exist." -ForegroundColor "Yellow"

                    }

                    # Console Line
                    "—" * $Host.UI.RawUI.WindowSize.Width

                    # Grab User Input as Variable
                    $TargetDestination = Read-Host -Prompt "Select Destination"

                    # User Input is Empty Variable
                    if ([String]::IsNullOrWhiteSpace($TargetDestination)) {
                        Remove-Variable -Name "TargetDestination" -ErrorAction "SilentlyContinue"
                    }

                    # User Input is Not Empty Variable
                    if (!([String]::IsNullOrWhiteSpace($TargetDestination))) {

                        # Expand User Input Variable
                        $TargetDestination = [System.IO.Path]::GetFullPath($ExecutionContext.InvokeCommand.ExpandString($TargetDestination))

                        # Set Current Directory as Default
                        if ($TargetDestination.ToLower().Contains("$env:SystemRoot\System32".ToLower())) {
                            $TargetDestination = [System.IO.Path]::GetFullPath($ExecutionContext.InvokeCommand.ExpandString($PWD.Path)) + "\" + [System.IO.Path]::GetFileName($TargetDestination)
                        }

                        # Extension Type Checker
                        if (!(([System.IO.Path]::GetExtension($TargetDestination)) -Match ".zip")) {
                            $TargetDestination = $TargetDestination + ".zip"
                        }

                        # String Format Validation
                        if (!($TargetDestination -Match "\\")) {
                            Remove-Variable -Name "TargetDestination" -ErrorAction "SilentlyContinue"
                            $UserInputError = $True
                        }

                        # File Path Validation
                        if (!(([System.IO.Path]::GetFileName($TargetDestination) -Match [Regex]::New("^[a-zA-Z0-9\ \:\'\-\[\]{}()+=_.,;@~#!£$€¥%^&×¤]+$")))) {
                            Remove-Variable -Name "TargetDestination" -ErrorAction "SilentlyContinue"
                            $UserInputError = $True
                        }

                    }

                }

                "F3" {

                    # Refresh Variable
                    Remove-Variable -Name "TerminateSelection" -ErrorAction "SilentlyContinue"

                    # Console Screen Refresh
                    [System.Console]::Clear()

                    # Console Line
                    "—" * $Host.UI.RawUI.WindowSize.Width

                    # Console Information
                    Write-Host "User Action Required"
                    Write-Host "Please enter a compression mode that will"
                    Write-Host "determine the overall size of the file."
                    Write-Host "Note that this will default to maximum."
                    Write-Host "• Press [F1] to set maximum compression."
                    Write-Host "• Press [F2] for fastest compression."
                    Write-Host "• Press [F3] to ignore compression."

                    # Console Line
                    "—" * $Host.UI.RawUI.WindowSize.Width

                    # Console Information
                    Write-Host "• Maximum - Processing time is dependent on folder size (smallest archive size, highest compression)."
                    Write-Host "• Fastest - Fastest compression method available to reduce processing time (medium archive size)."
                    Write-Host "• Ignored - Doesn't compress the source files (largest archive size, no compression)."

                    # Console Line
                    "—" * $Host.UI.RawUI.WindowSize.Width

                    while (!($TerminateSelection)) {

                        # User Deterime an Option
                        switch ([System.Console]::ReadKey().Key) {
                            "F1" {
                                $CompressionLevel = [System.IO.Compression.CompressionLevel]::Optimal
                                $CompressionMode = "Maximum"
                                $TerminateSelection = $True
                            }
                            "F2" {
                                $CompressionLevel = [System.IO.Compression.CompressionLevel]::Fastest
                                $CompressionMode = "Fastest"
                                $TerminateSelection = $True
                            }
                            "F3" {
                                $CompressionLevel = [System.IO.Compression.CompressionLevel]::NoCompression
                                $CompressionMode = "Ignored"
                                $TerminateSelection = $True
                            }
                            "ENTER" {
                                Remove-Variable -Name "CompressionLevel"
                                Remove-Variable -Name "CompressionMode"
                                $TerminateSelection = $True
                            }
                            "BACKSPACE" {
                                Remove-Variable -Name "CompressionLevel"
                                Remove-Variable -Name "CompressionMode"
                                $TerminateSelection = $True
                            }
                        }

                        # Console Pause
                        [Threading.Thread]::Sleep(500)

                    }

                }

                "F4" {

                    # Toggle Variable
                    if ([String]::IsNullOrEmpty($ValidateMode)) {
                        $ValidateMode = "Enabled"
                    } elseif ($ValidateMode -EQ "Enabled") {
                        $ValidateMode = "Strict"
                    } elseif ($ValidateMode -EQ "Strict") {
                        Remove-Variable -Name "ValidateMode" -ErrorAction "SilentlyContinue"
                    }

                }

                "F5" {

                    # Toggle Variable
                    if (([String]::IsNullOrEmpty($DeleteMode)) -AND ([String]::IsNullOrEmpty($RecycleMode))) {
                        $DeleteMode = $True
                    } elseif (($DeleteMode -EQ $True) -AND ([String]::IsNullOrEmpty($RecycleMode))) {
                        Remove-Variable -Name "DeleteMode" -ErrorAction "SilentlyContinue"
                        $RecycleMode = $True
                    } elseif (!([String]::IsNullOrEmpty($RecycleMode))) {
                        Remove-Variable -Name "RecycleMode" -ErrorAction "SilentlyContinue"
                    }

                }

                "F6" {

                    # Activate Running Mode
                    if (!([String]::IsNullOrEmpty($TargetPath))) {
                        $RunningMode = $True
                    }

                }

                "F12" {

                    # System Tray Icon Removal
                    $TrayIcon.Visible = $False

                    # Terminate Running Script
                    $TerminateMode = $True

                }

            }

        }

    }

    while ($RunningMode -EQ $True) {

        if (!($ProgramExecuted -EQ $True)) {

            # Define Variable
            $ProgramExecuted = $True

            # Console Screen Refresh
            [System.Console]::Clear()

            # Console Line
            "—" * $Host.UI.RawUI.WindowSize.Width

            # Console Information
            Write-Host "Folder Compression Started"
            Write-Host "• Please wait until the process has finished."
            Write-Host "• Exiting will result in a corrupt archive."

            # Console Line
            "—" * $Host.UI.RawUI.WindowSize.Width

            # Target Destination Default Value
            if ([String]::IsNullOrEmpty($TargetDestination)) {
                $TargetDestination = [System.IO.Path]::GetDirectoryName($TargetPath) + "\" + [System.IO.Path]::GetFileName($TargetPath) + ".zip"
            }

            # Remove Target Destination
            if (([System.IO.FileInfo]$TargetDestination).Exists) {
                Remove-Item -Path $TargetDestination -Force -Recurse
            }

            # Compression Level Default Value
            if ([String]::IsNullOrEmpty($CompressionLevel)) {
                $CompressionLevel = [System.IO.Compression.CompressionLevel]::Optimal
            }

            # Load Required Resources for Compression and Extraction
            [Reflection.Assembly]::LoadWithPartialName("System.IO.Compression.FileSystem") | Out-Null

            # Thoughts on Percent Complete and Continuation from Last Session
            # • System tray icon with progress percent requires update compression method instead of create from directory.
            # • Can also continue where left off as can check current files in archive.

            # Execute Folder Compression
            [System.IO.Compression.ZipFile]::CreateFromDirectory($TargetPath, $TargetDestination, $CompressionLevel, $False)

            # Execute Validation Mode
            if (($ValidateMode -EQ "Enabled") -OR ($ValidateMode -EQ "Strict")) {

                # Console Information
                Write-Host "Archive Validation Started"
                Write-Host "• Extracting files to a temporary directory."
                Write-Host "• Comparing the generated files to the source."

                # Console Line
                "—" * $Host.UI.RawUI.WindowSize.Width

                # Strict Matching Information
                # • Recommened to have disabled as some compressed folders can contain empty folders.
                # • Empty folders add to the count of items in the source.

                # Create Error List
                $ValidationErrorList = [System.Collections.Generic.List[Object]]::New()

                # Define Validation Source
                $ValidationSource = [System.IO.Path]::GetDirectoryName($TargetDestination) + "\" + (([System.Guid]::NewGuid()).Guid).ToUpper().Substring(0,8)

                # Rename Original Folder
                if (([System.IO.FileInfo]$ValidationSource).Exists) {
                    $ValidationRename = $True
                    Rename-Item -Path "$ValidationSource" -NewName "$ValidationSource (Original Folder)" -Force
                }

                # Create Validation Folder
                New-Item -Path $ValidationSource -ItemType "Directory" -Force | Out-Null

                # Hide Validation Folder
                Get-Item $ValidationSource -Force | Where-Object {$_.Attributes = $_.Attributes -BOR ([System.IO.FileAttributes]::Hidden)}
                Get-Item $ValidationSource -Force | Where-Object {$_.Attributes = $_.Attributes -BOR ([System.IO.FileAttributes]::System)}

                # Extract Compressed File to Validation Source
                [System.IO.Compression.ZipFile]::ExtractToDirectory($TargetDestination, $ValidationSource)

                # Compressed File Existance Checker
                if (!(([System.IO.FileInfo]$TargetDestination).Exists)) {

                    # Set Error Mode
                    $ValidationError = $True
                    [Void]$ValidationErrorList.Add("Compressed folder does not exist.")

                # Execute in Event of Folder Existance
                } elseif (([System.IO.FileInfo]$TargetDestination).Exists) {

                    # Exclude Hidden Elements Information
                    # • Get-ChildItem -Attributes "!System" - Exclude System Protected Elements
                    # • Get-ChildItem -Attributes "!Hidden" - Exclude Normal Hidden Elements

                    # Define Source Folder Measurements
                    $SourceElement = Get-ChildItem -Path $TargetPath -Force -Recurse
                    $SourceMeasurement = $SourceElement | Measure-Object -Property "Length" -Sum -Average -Maximum -Minimum

                    # Define Validation Source Measurements
                    $ValidationElement = Get-ChildItem -Path $ValidationSource -Force -Recurse
                    $ValidationMeasurement = $ValidationElement | Measure-Object -Property "Length" -Sum -Average -Maximum -Minimum

                    # Strict Matching Component
                    if ($ValidateMode -EQ "Strict") {

                        # Element Count Checker
                        if (!($SourceMeasurement.Count -EQ $ValidationMeasurement.Count)) {

                            # Set Error Mode
                            $ValidationError = $True
                            [Void]$ValidationErrorList.Add("Element count mismatch between source and validation folder.")

                        }

                    }

                    # Element Size Checker
                    if (!($SourceMeasurement.Sum -EQ $ValidationMeasurement.Sum)) {

                        # Set Error Mode
                        $ValidationError = $True
                        [Void]$ValidationErrorList.Add("Total size between source and validation folder is different.")

                    }

                    # Run if Folders are Populated
                    if (($SourceElement.Length -NE 0) -AND ($ValidationElement.Length -NE 0)) {

                        # Run Componenet if No Errors
                        if ($ValidationError -NE $True) {

                            # Strict Matching Component
                            if ($ValidateMode -EQ "Strict") {

                                # Grab Comparison Table of Basic File Attributes
                                $Difference = Compare-Object -ReferenceObject $SourceElement -DifferenceObject $ValidationElement -Property @("Name", "Length") -IncludeEqual -CaseSensitive -PassThru

                                foreach ($Element in $Difference) {

                                    # Calculate Element Size in Megabytes
                                    $ElementSize = (($Element.Length/1024)/1024)

                                    # Unique to Difference Object
                                    if ($Element.SideIndicator -EQ "=>") {

                                        # Set Error Mode
                                        $ValidationError = $True
                                        [Void]$ValidationErrorList.Add("$($Element.Name) [$($Element.Length)] is unique to validation folder.")

                                    }

                                    # Unique to Reference Object
                                    if ($Element.SideIndicator -EQ "<=") {

                                        $ValidationError = $True
                                        [Void]$ValidationErrorList.Add("$($Element.Name) [$($Element.Length)] is unique to source folder.")

                                    }

                                }

                            }

                        }

                        # Run Componenet if No Errors
                        if ($ValidationError -NE $True) {

                            # Grab Comparison Table of Advanced File Attributes
                            $Difference = Compare-Object -ReferenceObject $SourceElement -DifferenceObject $ValidationElement -Property @("Name", "FullName", "Length", "LastWriteTime") -IncludeEqual -CaseSensitive -PassThru

                            # Create Hash List
                            $SourceHashList = [System.Collections.Generic.List[Object]]::New()
                            $ValidationHashList = [System.Collections.Generic.List[Object]]::New()

                            # Set Hash Index
                            $SourceHashIndex = 0
                            $ValidationHashIndex = 0

                            foreach ($Element in $Difference) {

                                # File is in Destination Folder
                                if (($Element.FullName).Contains($TargetPath)) {

                                    # Increment the Index
                                    $SourceHashIndex = $SourceHashIndex + 1

                                    # Strict Matching Component
                                    if ($ValidateMode -EQ "Strict") {

                                        # Element is a Folder
                                        if (([System.IO.DirectoryInfo]$Element.FullName).Exists) {

                                            # Folder Element Count
                                            $ElementCount = [System.IO.Directory]::GetFiles($Element.FullName).Count

                                            # Add to Hash List
                                            [Void]$SourceHashList.Add("[$SourceHashIndex] [$($Element.Name)] [$($ElementCount)] [$($Element.LastWriteTime)]")

                                        # Element is a File
                                        } elseif (([System.IO.FileInfo]$Element.FullName).Exists) {

                                            # Grab File Hash
                                            $FileHash = Get-FileHash $Element.FullName -Algorithm "SHA256" | Select-Object -ExpandProperty "Hash"

                                            # Add to Hash List
                                            [Void]$SourceHashList.Add("[$SourceHashIndex] [$($Element.Name)] [$($Element.Length)] [$($Element.LastWriteTime)] [$FileHash]")

                                        }

                                    # Unstrict Matching Component
                                    } else {

                                        # Element is a Folder
                                        if (([System.IO.DirectoryInfo]$Element.FullName).Exists) {

                                            # Folder Element Count
                                            $ElementCount = [System.IO.Directory]::GetFiles($Element.FullName).Count

                                            # Add to Hash List
                                            [Void]$SourceHashList.Add("[$($Element.Name)] [$($ElementCount)]")

                                        # Element is a File
                                        } elseif (([System.IO.FileInfo]$Element.FullName).Exists) {

                                            # Grab File Hash
                                            $FileHash = Get-FileHash $Element.FullName -Algorithm "SHA256" | Select-Object -ExpandProperty "Hash"

                                            # Add to Hash List
                                            [Void]$SourceHashList.Add("[$($Element.Name)] [$($Element.Length)] [$FileHash]")

                                        }

                                    }

                                # File is in Validation Source
                                } elseif (($Element.FullName).Contains($ValidationSource)) {

                                    # Increment the Index
                                    $ValidationHashIndex = $ValidationHashIndex + 1

                                    # Strict Matching Component
                                    if ($ValidateMode -EQ "Strict") {

                                        # Element is a Folder
                                        if (([System.IO.DirectoryInfo]$Element.FullName).Exists) {

                                            # Folder Element Count
                                            $ElementCount = [System.IO.Directory]::GetFiles($Element.FullName).Count

                                            # Add to Hash List
                                            [Void]$ValidationHashList.Add("[$ValidationHashIndex] [$($Element.Name)] [$($ElementCount)] [$($Element.LastWriteTime)]")

                                        # Element is a File
                                        } elseif (([System.IO.FileInfo]$Element.FullName).Exists) {

                                            # Grab File Hash
                                            $FileHash = Get-FileHash $Element.FullName -Algorithm "SHA256" | Select-Object -ExpandProperty "Hash"

                                            # Add to Hash List
                                            [Void]$ValidationHashList.Add("[$ValidationHashIndex] [$($Element.Name)] [$($Element.Length)] [$($Element.LastWriteTime)] [$FileHash]")

                                        }

                                    # Unstrict Matching Component
                                    } else {

                                        # Element is a Folder
                                        if (([System.IO.DirectoryInfo]$Element.FullName).Exists) {

                                            # Folder Element Count
                                            $ElementCount = [System.IO.Directory]::GetFiles($Element.FullName).Count

                                            # Add to Hash List
                                            [Void]$ValidationHashList.Add("[$($Element.Name)] [$($ElementCount)]")

                                        # Element is a File
                                        } elseif (([System.IO.FileInfo]$Element.FullName).Exists) {

                                            # Grab File Hash
                                            $FileHash = Get-FileHash $Element.FullName -Algorithm "SHA256" | Select-Object -ExpandProperty "Hash"

                                            # Add to Hash List
                                            [Void]$ValidationHashList.Add("[$($Element.Name)] [$($Element.Length)] [$FileHash]")

                                        }

                                    }

                                }

                            }

                        }

                    }

                    # Run Componenet if No Errors
                    if ($ValidationError -NE $True) {

                        # Strict Matching Component
                        if ($ValidateMode -EQ "Strict") {

                            # List Length Checker
                            if ($SourceHashList.Count -NE $ValidationHashList.Count) {
                                $ValidationError = $True
                                [Void]$ValidationErrorList.Add("Quantity of elements in source hash list and validation hash are different.")
                            }

                        }

                    }

                    # Run Componenet if No Errors
                    if ($ValidationError -NE $True) {

                        # Compare Validation List to Source List
                        foreach ($Element in $SourceHashList) {
                            if ($ValidationHashList -NotContains $Element) {
                                $ValidationError = $True
                                [Void]$ValidationErrorList.Add("Could not find $Element in source hash list.")
                            }
                        }

                        # Compare Source List to Validation List
                        foreach ($Element in $ValidationHashList) {
                            if ($SourceHashList -NotContains $Element) {
                                $ValidationError = $True
                                [Void]$ValidationErrorList.Add("Could not find $Element in validation hash list.")
                            }
                        }

                    }

                    if ($False) {

                        [String[]]$Result1 = $SourceElement.FullName
                        [String[]]$Result2 = $ValidationElement.FullName

                        $Result1List = [System.Collections.Generic.List[Object]]::New()
                        $Result2List = [System.Collections.Generic.List[Object]]::New()

                        [System.Linq.Enumerable]::Except($Result1, $Result2) | ForEach-Object {
                            if (($_).Contains($TargetPath)) {
                                [Void]$Result1List.Add($_ -Replace ([Regex]::Escape("$TargetPath" + "\"), ""))
                            }
                        }

                        [System.Linq.Enumerable]::Except($Result2, $Result1) | ForEach-Object {
                            if (($_).Contains($ValidationSource)) {
                                [Void]$Result2List.Add($_ -Replace ([Regex]::Escape("$ValidationSource" + "\"), ""))
                            }
                        }

                        $Result1ListDiff = [System.Collections.Generic.List[Object]]::New()
                        $Result2ListDiff = [System.Collections.Generic.List[Object]]::New()

                        foreach ($Element in $Result1List) {
                            if ($Result2List -NotContains $Element) {
                                [Void]$Result1ListDiff.Add($Element)
                            }
                        }

                        foreach ($Element in $Result2List) {
                            if ($Result1List -NotContains $Element) {
                                [Void]$Result2ListDiff.Add($Element)
                            }
                        }

                        if ($Result1ListDiff.Count -NE 0) {
                            Write-Host "Unique to Source Folder"
                            $Result1ListDiff | ForEach-Object {
                                Write-Host "• $_"
                            }
                        }

                        if ($Result2ListDiff.Count -NE 0) {
                            Write-Host "Unique to Validation Folder"
                            $Result2ListDiff | ForEach-Object {
                                Write-Host "• $_"
                            }
                        }

                    }

                }

                # Remove Validation Source
                if (([System.IO.DirectoryInfo]$ValidationSource).Exists) {
                    Remove-Item -Path $ValidationSource -Force -Recurse
                }

                # Rename Temporary Folder
                if (!([String]::IsNullOrEmpty($ValidationRename))) {
                    Rename-Item -Path "$ValidationSource (Original Folder)" -NewName "$ValidationSource" -Force
                }

                if ([String]::IsNullOrEmpty($ValidationError)) {

                    # Console Information
                    Write-Host "Archive Validation Success"
                    Write-Host "• Detected a successful archive creation."
                    Write-Host "• Temporary folder aligned with destination."

                    # Console Line
                    "—" * $Host.UI.RawUI.WindowSize.Width

                } else {

                    # Console Information
                    Write-Host "Archive Validation Failure" -ForegroundColor "Red"
                    Write-Host "• Detected a unsuccessful archive creation." -ForegroundColor "Red"
                    Write-Host "• Temporary folder did not align with destination." -ForegroundColor "Red"

                    if (($ValidateMode -EQ "Strict") -OR ($VerboseMode -EQ $True)) {

                        $ValidationErrorList | ForEach-Object {

                            # Console Information
                            Write-Host "• $_" -ForegroundColor "Red"

                        }

                    }

                    # Console Line
                    "—" * $Host.UI.RawUI.WindowSize.Width

                }

            }

            # Execute Deletion Mode
            if (($DeleteMode -EQ $True) -OR ($RecycleMode -EQ $True)) {

                # Execute in Event of Successful or No Validation
                if ([String]::IsNullOrEmpty($ValidationError)) {

                    # Console Information
                    Write-Host "Deletion Announcement" -ForegroundColor "Yellow"
                    Write-Host "Please note that the source folder has been marked for deletion." -ForegroundColor "Yellow"
                    Write-Host "You can exit the program now to prevent the deletion of folder." -ForegroundColor "Yellow"
                    Write-Host "• Folder Selected: $TargetPath" -ForegroundColor "Yellow"
                    Write-Host "• Press [Enter] to continue with deletion." -ForegroundColor "Yellow"

                    # Console Line
                    "—" * $Host.UI.RawUI.WindowSize.Width

                    # Await User Input
                    $Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown") | Out-Null

                    # Delete Mode Activated
                    if ($DeleteMode -EQ $True) {

                        # Delete Target Source upon Archive Creation
                        if (([System.IO.FileInfo]$TargetDestination).Exists) {

                            # Permanently Delete the Folder and Subfolders
                            Remove-Item -Path $TargetPath -Force -Recurse

                            # Console Information
                            Write-Host "Source Deletion Success"
                            Write-Host "• Source folder was deleted as deletion mode was enabled."
                            Write-Host "• Uncompress the archive file to return to original state."

                        }

                    }

                    # Recycle Mode Activated
                    if ($RecycleMode -EQ $True) {

                        # Delete Target Source upon Archive Creation
                        if (([System.IO.FileInfo]$TargetDestination).Exists) {

                            # Move Files to Recycle Bin
                            ((New-Object -ComObject "Shell.Application").Namespace(0).ParseName($TargetPath)).InvokeVerb("Delete")

                            # Console Information
                            Write-Host "Source Deletion Success"
                            Write-Host "• Source folder was moved to recycle bin as recycle mode was enabled."
                            Write-Host "• Uncompress the archive file to return to original state."

                        }

                    }

                    if (([System.IO.DirectoryInfo]$TargetPath).Exists) {

                        # Console Information
                        Write-Host "Source Deletion Failure" -ForegroundColor "Red"
                        Write-Host "• Detected that the source folder was not deleted." -ForegroundColor "Red"
                        Write-Host "• Deletion is cancelled when unable to locate output." -ForegroundColor "Red"

                    }

                    # Console Line
                    "—" * $Host.UI.RawUI.WindowSize.Width

                } else {

                    # Console Information
                    Write-Host "Source Deletion Failure" -ForegroundColor "Red"
                    Write-Host "• Source folder was not deleted due to validation failure." -ForegroundColor "Red"
                    Write-Host "• Please close all background programs and try again." -ForegroundColor "Red"

                    # Console Line
                    "—" * $Host.UI.RawUI.WindowSize.Width

                }

            }

            # Console Information
            Write-Host "Folder Compression Finished"
            Write-Host "• Press [F1] to return to main menu."
            Write-Host "• Press [F2] to close the terminal."

            # Console Line
            "—" * $Host.UI.RawUI.WindowSize.Width

            # Define Variable
            $RunningMode = $False

        }

        while (!($MarkedComplete -EQ $True)) {

            if ([Console]::KeyAvailable) {

                switch ([System.Console]::ReadKey().Key) {

                    "F1" {

                        # Return to Main Menu
                        $MarkedComplete = $True

                        # Master Variable Refresh
                        $PowerShell = [PowerShell]::Create()
                        $PowerShell.AddScript('Get-Variable | Select-Object -ExpandProperty "Name"') | Out-Null
                        $VariableList = $PowerShell.Invoke()
                        $PowerShell.Dispose()
                        $VariableList = Get-Variable | Select-Object -ExpandProperty "Name" | Where-Object { $VariableList -NotContains $_ }

                        # Define Ignore List
                        $ExemptionList = [System.Collections.Generic.List[Object]]::New()
                        [Void]$ExemptionList.Add("ErrorActionPreference")
                        [Void]$ExemptionList.Add("OutputEncoding")
                        [Void]$ExemptionList.Add("VariableList")
                        [Void]$ExemptionList.Add("ExemptionList")
                        [Void]$ExemptionList.Add("MarkedComplete")
                        [Void]$ExemptionList.Add("TargetPath")
                        [Void]$ExemptionList.Add("TargetDestination")
                        [Void]$ExemptionList.Add("CompressionLevel")
                        [Void]$ExemptionList.Add("CompressionMode")
                        [Void]$ExemptionList.Add("ValidateMode")
                        [Void]$ExemptionList.Add("DeleteMode")
                        [Void]$ExemptionList.Add("RecycleMode")
                        [Void]$ExemptionList.Add("TrayIcon")

                        # Remove Variable
                        ForEach ($Variable in $VariableList) {
                            try {
                                if (!($ExemptionList.Contains($Variable))) {
                                    Remove-Variable -Name $Variable -ErrorAction "SilentlyContinue"
                                }
                            } catch {
                                $RunningMode = $False
                                $MarkedComplete = $True
                                [System.Windows.Forms.SendKeys]::SendWait("{ENTER}")
                            }
                        }

                    }

                    "F2" {

                        # System Tray Icon Removal
                        $TrayIcon.Visible = $False

                        # Terminate Process
                        Stop-Process -ID $PID

                    }

                    "ENTER" {

                        # Return to Main Menu
                        $MarkedComplete = $True

                        # Master Variable Refresh
                        $PowerShell = [PowerShell]::Create()
                        $PowerShell.AddScript('Get-Variable | Select-Object -ExpandProperty "Name"') | Out-Null
                        $VariableList = $PowerShell.Invoke()
                        $PowerShell.Dispose()
                        $VariableList = Get-Variable | Select-Object -ExpandProperty "Name" | Where-Object { $VariableList -NotContains $_ }

                        # Define Ignore List
                        $ExemptionList = [System.Collections.Generic.List[Object]]::New()
                        [Void]$ExemptionList.Add("ErrorActionPreference")
                        [Void]$ExemptionList.Add("OutputEncoding")
                        [Void]$ExemptionList.Add("VariableList")
                        [Void]$ExemptionList.Add("ExemptionList")
                        [Void]$ExemptionList.Add("MarkedComplete")
                        [Void]$ExemptionList.Add("TargetPath")
                        [Void]$ExemptionList.Add("TargetDestination")
                        [Void]$ExemptionList.Add("CompressionLevel")
                        [Void]$ExemptionList.Add("CompressionMode")
                        [Void]$ExemptionList.Add("ValidateMode")
                        [Void]$ExemptionList.Add("DeleteMode")
                        [Void]$ExemptionList.Add("RecycleMode")
                        [Void]$ExemptionList.Add("TrayIcon")

                        # Remove Variable
                        ForEach ($Variable in $VariableList) {
                            try {
                                if (!($ExemptionList.Contains($Variable))) {
                                    Remove-Variable -Name $Variable -ErrorAction "SilentlyContinue"
                                }
                            } catch {
                                $RunningMode = $False
                                $MarkedComplete = $True
                                [System.Windows.Forms.SendKeys]::SendWait("{ENTER}")
                            }
                        }

                    }

                    "BACKSPACE" {

                        # System Tray Icon Removal
                        $TrayIcon.Visible = $False

                        # Terminate Process
                        Stop-Process -ID $PID

                    }

                    "F12" {

                        # System Tray Icon Removal
                        $TrayIcon.Visible = $False

                        # Exit Running Mode
                        $RunningMode = $False
                        $MarkedComplete = $True

                        # Terminate Running Script
                        $TerminateMode = $True

                    }

                }

            }

            # Console Pause
            [Threading.Thread]::Sleep(500)

        }

        # Reset Variable
        Remove-Variable -Name "MarkedComplete" -ErrorAction "SilentlyContinue"

    }

}